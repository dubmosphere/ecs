namespace ecs {

template <class Type>
unsigned long long EntityManager::getTypeHash()
{
    return typeid(Type).hash_code();
}

} // namespace ecs
