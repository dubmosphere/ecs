#ifndef ECS_ENTITY_HPP
#define ECS_ENTITY_HPP

// ECS includes
#include "ECS/EntityManager.hpp"
#include "ECS/Component.hpp"

// STL includes
#include <unordered_map>

namespace ecs {

/**
 * The Entity class holds the components. In real Entity Component Systems this would only be an ID,\n
 * but for simplicity I'll do that as a class.
 */
class Entity
{
    public:
        /** \brief Constructor
         *
         * \param const unsigned long long& eid
         */
        Entity(const unsigned long long eid);

        unsigned long long getEid();

        void destroy();
        bool isDestroyed();

        bool hasComponent(const unsigned long long typeHash);

        template <class ComponentType>
        bool hasComponent();

        template <class ComponentType>
        ComponentType* addComponent();

        template <class ComponentType>
        void removeComponent();

        template <class ComponentType>
        ComponentType* getComponent();

    private:
        const unsigned long long eid;
        bool destroyed;
        std::unordered_map<unsigned long long, std::unique_ptr<Component>> components;
};

} // namespace ecs

#include "ECS/Entity.inl"

#endif // ECS_ENTITY_HPP
