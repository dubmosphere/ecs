namespace ecs {

template<class SystemType>
void SystemManager::addSystem()
{
    if(!hasSystem<SystemType>()) {
        systems.push_back(std::make_unique<SystemType>(entityManager));
    }
}

template<class SystemType>
bool SystemManager::hasSystem() {
    return std::find_if(std::begin(systems), std::end(systems), [&](const auto &system) {
        EntityManager::getTypeHash<SystemType>() == typeid(*system).hash_code();
    }) != std::end(systems);
}

} // namespace ecs
