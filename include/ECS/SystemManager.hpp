#ifndef ECS_SYSTEMMANAGER_HPP
#define ECS_SYSTEMMANAGER_HPP

// ECS includes
#include "ECS/EntityManager.hpp"
#include "ECS/System.hpp"

namespace ecs {

class System;

class SystemManager
{
    public:
        SystemManager(EntityManager& entityManager);
        void update(float dt);

        template<class SystemType>
        void addSystem();

        template<class SystemType>
        bool hasSystem();

    private:
        EntityManager& entityManager;
        std::vector<std::unique_ptr<System>> systems;
};

} // namespace ecs

#include "ECS/SystemManager.inl"

#endif // ECS_SYSTEMMANAGER_HPP
