namespace ecs {

template <class ComponentType>
void System::require()
{
    const unsigned long long typeHash = EntityManager::getTypeHash<ComponentType>();
    auto it = std::find(std::begin(componentTypeHashes), std::end(componentTypeHashes), typeHash);

    if(it == std::end(componentTypeHashes)) {
        componentTypeHashes.push_back(typeHash);
    }
}

} // namespace ecs
