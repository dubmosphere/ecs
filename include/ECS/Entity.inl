namespace ecs {

template <class ComponentType>
bool Entity::hasComponent()
{
    return hasComponent(EntityManager::getTypeHash<ComponentType>());
}

template <class ComponentType>
ComponentType* Entity::addComponent()
{
    if(!hasComponent<ComponentType>()) {
        components.emplace(EntityManager::getTypeHash<ComponentType>(), std::make_unique<ComponentType>());
    }

    return getComponent<ComponentType>();
}

template <class ComponentType>
void Entity::removeComponent()
{
    if(hasComponent<ComponentType>()) {
        components.erase(EntityManager::getTypeHash<ComponentType>());
    }
}

template <class ComponentType>
ComponentType* Entity::getComponent()
{
    return static_cast<ComponentType*>(components.at(EntityManager::getTypeHash<ComponentType>()).get());
}

} // namespace ecs
