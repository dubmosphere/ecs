#ifndef ECS_ENTITYFACTORY_HPP
#define ECS_ENTITYFACTORY_HPP

namespace ecs {

class EntityManager;
class Entity;

class EntityFactory {
    protected:
        EntityFactory(EntityManager& entityManager);
        virtual ~EntityFactory();

        Entity* createEntity();

    private:
        EntityManager& entityManager;
};

} // namespace ecs

#endif // ECS_ENTITYFACTORY_HPP
