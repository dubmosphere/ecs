#ifndef ECS_ENTITYMANAGER_HPP
#define ECS_ENTITYMANAGER_HPP

// STL includes
#include <vector>
#include <memory>
#include <typeinfo>

namespace ecs {

class SystemManager;
class Entity;

class EntityManager
{
    public:
        EntityManager();
        void update(float dt);
        Entity* createEntity();

        SystemManager& getSystemManager();

        const std::vector<Entity*> findByComponentTypeHashes(const std::vector<unsigned long long>& typeHashes) const;
        const std::vector<std::unique_ptr<Entity>>& findAll() const;
        void destroy(Entity* entity);

        template <class Type>
        static unsigned long long getTypeHash();

    private:
        unsigned long long lastEid;
        std::vector<std::unique_ptr<Entity>> entities;
        bool hasEntitiesToDestroy;
        std::unique_ptr<SystemManager> systemManager;
};

} // namespace ecs

#include "ECS/EntityManager.inl"

#endif // ECS_ENTITYMANAGER_HPP
