// Class includes
#include "ECS/Entity.hpp"

namespace ecs {

Entity::Entity(const unsigned long long eid) :
eid(eid),
destroyed(false)
{
}

unsigned long long Entity::getEid()
{
    return eid;
}

void Entity::destroy()
{
    destroyed = true;
}

bool Entity::isDestroyed()
{
    return destroyed;
}

bool Entity::hasComponent(const unsigned long long typeHash)
{
    auto it = components.find(typeHash);

    return it != std::end(components);
}

} // namespace ecs
