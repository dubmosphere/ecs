// Class includes
#include "ECS/SystemManager.hpp"

// ECS includes
#include "ECS/System.hpp"

namespace ecs {

SystemManager::SystemManager(EntityManager& entityManager) :
entityManager(entityManager)
{
}

void SystemManager::update(float dt)
{
	for(auto &system : systems) {
		system->update(dt);
	}
}

} // namespace ecs
