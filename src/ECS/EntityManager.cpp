// Class includes
#include "ECS/EntityManager.hpp"

// ECS includes
#include "ECS/Entity.hpp"
#include "ECS/SystemManager.hpp"

namespace ecs {

EntityManager::EntityManager() :
lastEid(0),
hasEntitiesToDestroy(false),
systemManager(std::make_unique<SystemManager>(*this))
{
}

void EntityManager::update(float dt)
{
    systemManager->update(dt);

    if(hasEntitiesToDestroy) {
        entities.erase(
            std::remove_if(std::begin(entities), std::end(entities), [](const auto& entity) {
                return entity.get()->isDestroyed();
            }),
            std::end(entities)
        );

        hasEntitiesToDestroy = false;
    }
}

Entity* EntityManager::createEntity()
{
    entities.push_back(std::make_unique<Entity>(++lastEid));

    return entities.back().get();
}

SystemManager& EntityManager::getSystemManager()
{
    return *systemManager.get();
}

const std::vector<Entity*> EntityManager::findByComponentTypeHashes(const std::vector<unsigned long long>& typeHashes) const
{
    std::vector<Entity*> filteredEntities;
    filteredEntities.reserve(entities.size());

	for(const auto &entity : entities) {
		bool validEntity = true;

		for(const auto &typeHash : typeHashes) {
			if(!entity.get()->hasComponent(typeHash)) {
				validEntity = false;
			}
		}

		if(validEntity) {
			filteredEntities.push_back(entity.get());
		}
	}

    return filteredEntities;
}

const std::vector<std::unique_ptr<Entity>>& EntityManager::findAll() const
{
    return entities;
}

void EntityManager::destroy(Entity* entity)
{
    entity->destroy();
    hasEntitiesToDestroy = true;
}

} // namespace ecs
