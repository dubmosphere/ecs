// Class includes
#include "ECS/EntityFactory.hpp"

// ECS includes
#include "ECS/EntityManager.hpp"

namespace ecs {

EntityFactory::EntityFactory(EntityManager& entityManager) :
entityManager(entityManager)
{
}

EntityFactory::~EntityFactory()
{
}

Entity* EntityFactory::createEntity()
{
    return entityManager.createEntity();
}

} // namespace ecs
